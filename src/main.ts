import Vue from 'vue';
import App from './App.vue';
import router from './router';
import ImgCrop from './plugins/ImgCrop';

Vue.config.productionTip = false;
Vue.use(ImgCrop);

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
