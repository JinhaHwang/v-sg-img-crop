module.exports = {
  // 라이브러리 빌드시 .ts또는 .js일 경우 default를 추출한다
  configureWebpack: {
    output: {
      libraryExport: 'default'
    }
  }
}