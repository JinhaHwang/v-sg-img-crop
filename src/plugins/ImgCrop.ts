import Vue from 'vue';
import ImgCrop from '../components/ImgCrop.vue';

export default class ImgCropPlugin {
  // vue-router/types/router.d.ts 에서 아래처럼 플러그인 작성
  public static install(v: typeof Vue, options?: any) {
    v.component('img-crop', ImgCrop);
  }
}
