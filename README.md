# 스마일게이트스토브 img-crop 과제
## 과제 내용
이미지 크롭 및 저장하는 부분을 img-crop 라이브러리화 하고
해당 img-crop 라이브러리를 사용하는 **App을 만든다.**


## 필수 구현 과제
- [ ] Vue + Typescript + RxJS
- [v] lib화 하여 재사용 가능하도록 제작 (**플러그인**)
- [ ] IE9 이상에서 동작 가능 하도록
- [ ] Todo 저장 방식은 localstorage 또는 SharedService
- [v] 이미지 업로드 구현은 업로드 전 썸네일 이미지 표시 이 때 이미지 확대축소 기능 제공 (**더블클릭시 줌인 줌아웃**)


## 환경 구성
@vue-cli 를 글로벌 설치하여 vue 스캐폴딩 프로젝트를 생성
```
$ vue create img-crop-client

// interactive 옵션
babel, tslint, jest, postcss, scss, router
```


## 디렉터리구조(@vue/cli가 만들어주는 기본 구조)
src/

-> components/

-> -> ImgCrop.vue (**이미지 컴포넌트**)

-> plugins/ (**플러그인을 작성할 폴더 추가**)

-> -> ImgCropPlugin.ts (**이미지크롭 컴포넌트를 플러그인으로 추출**)

-> views/

-> -> Home.vue (**이미지크롭 플러그인 사용**)

main.ts (**플러그인 install**)

package.json (**라이브러리 빌드 스크립트 추가, prettier 설정**)

postcss.config.js (**scss로드시 autoprefixer적용**)

tslint.json (**기본tslint설정에 prettier config 추가**)

vue.config.js (**vue-cli-service로 라이브러리 추출시 사용할 설정**)


## 사용한 패키지
1. cropperjs (이미지 크롭 라이브러리)
2. @types/cropperjs (typescript definition)
3. autoprefixer
4. postcss-flexbugs-fixes (추가하긴 했으나 flex를 사용하진 않음)
5. prettier, tslint-config-prettier (tslint 포맷팅을 위한)


## 구현설명
ImgCrop 컴포넌트는 사용하는 부모 컴포넌트에서 <img-crop> 으로 사용 가능하도록
src_plugins_ImgCrop.ts 파일에서 install 함수를 구현
``` typescript
import Vue from 'vue';
import ImgCrop from '../components/ImgCrop.vue';

export default class ImgCropPlugin {
  // vue-router/types/router.d.ts 에서 아래처럼 플러그인 작성
  public static install(v: typeof Vue, options?: any) {
    v.component('img-crop', ImgCrop);
  }
}

```

ImgCrop 플러그인을 main.ts에서 플러그인설치
``` typescript
import ImgCrop from './plugins/ImgCrop';
Vue.use(ImgCrop);
```

Home.vue 에서 <img-crop>을 사용
``` html
<template>
    <div id="contents">
        <div class="title-wrap">
            <span>{{ title }}</span>
            <button class="close-btn" @click="close">X</button>
        </div>
        <div class="hint-wrap">
            <span class="hint">{{ hint }}</span>
        </div>
        <div v-if="isActiveImageCrop" class="img-crop-wrap" >
            <div class="img-crop">
                <img-crop :data="loadData" ref="imgCrop" />
            </div>
            <div class="control-wrap">
                <button @click="activeImageCrop(false)">취소</button>
                <button class="confirm" @click="confirm">확인</button>
            </div>
        </div>
        <div v-else class="img-crop-wrap">
            <div class="img-crop">
                <img :src="loadData.url" alt="이미지">
            </div>
            <div class="control-wrap">
                <input type="file" ref="file" accept="image/*" @change="change">
            </div>
        </div>

    </div>
</template>
```

data.isActiveImageCrop에 따라 이미지 태그를 보일지 크롭을 보일지 결정
``` html
<div v-if="isActiveImageCrop" class="img-crop-wrap" >
            <div class="img-crop">
                <img-crop :data="loadData" ref="imgCrop" />
            </div>
            <div class="control-wrap">
                <button @click="activeImageCrop(false)">취소</button>
                <button class="confirm" @click="confirm">확인</button>
            </div>
        </div>
        <div v-else class="img-crop-wrap">
            <div class="img-crop">
                <img :src="loadData.url" alt="이미지">
            </div>
            <div class="control-wrap">
                <input type="file" ref="file" accept="image/*" @change="change">
            </div>
        </div>
```

ImgCrop 컴포넌트 템플릿
``` html
<template>
    <div @dblclick="dblClick">
        <img ref="image" :src="data.url" :alt="data.name" @load="start">
    </div>
</template>
```


## 부족한점
외부 라이브러리 이용시 es6에서 typescript로 변경작업이 미흡했음.
단위테스트 부재.
RxJS 사용해보지 못했음. 이미지 파일을 로드 하는 부분에서 활용할 수 있었을것 같음.
localStroage를 사용하진 못했음.
